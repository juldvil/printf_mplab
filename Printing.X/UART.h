/* 
 * File:   UART.h
 * Author: Julie
 *
 * Created on 14 de octubre de 2020, 10:21 AM
 */

void init_uart()
{
    // Initialising the UART
    TXSTAbits.TXEN = 1;     // Enable Transmitter
    RCSTAbits.SPEN = 1;     // Enable Serial Port
}

void putch(unsigned char data)
{
    // The putch() Function
    while(!PIR1bits.TXIF)
        continue;
    TXREG = data;
}
